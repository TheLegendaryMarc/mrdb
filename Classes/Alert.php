<?php
	
	/**
	 * @author     Marc Retz
	 * @company    Beyond Media GmbH
	 */
	class Alert
	{
		/**
		 * @var string
		 */
		protected $state = 'success';
		
		/**
		 * @var string
		 */
		protected $message = '';
		
		/**
		 * Alert constructor.
		 *
		 * @param null|string $state
		 * @param null|string $message
		 */
		public function __construct($state = null, $message = null)
		{
			$this->setState($state);
			$this->setMessage($message);
		}
		
		/**
		 * @return string
		 */
		public function getState()
		{
			return $this->state;
		}
		
		/**
		 * @param string $state
		 *
		 * @return $this
		 */
		public function setState($state)
		{
			$this->state = $state;
			
			return $this;
		}
		
		/**
		 * @return string
		 */
		public function getMessage()
		{
			return $this->message;
		}
		
		/**
		 * @param string $message
		 *
		 * @return $this
		 */
		public function setMessage($message)
		{
			$this->message = $message;
			
			return $this;
		}
	}