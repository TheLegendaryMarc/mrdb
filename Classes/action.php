<?php
	
	class action extends Base
	{
		function __construct()
		{
			$user = new user();
			if (isset($_GET['logout'])) {
				$user->logout();
				exit;
			}
			if (isset($_GET['administration'])) {
				require("Classes/admin.php");
				$admin = new Admin();
				if ($admin->isadmin()) {
					include("seiten/admin.phtml");
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diesen Bereich");
				}
				exit;
			}
			if (isset($_GET['account'])) {
				$user->showAccount();
				exit;
			}
			
			if (isset($_GET['createuser'])) {
				require("Classes/admin.php");
				$admin = new admin();
				$admin->isadmin();
				$admin->createUser();
				exit;
			}
			if (isset($_GET['daten'])) {
				include("seiten/daten.phtml");
				exit;
			}
			
			if (isset($_GET['removeuser'])) {
				require("Classes/admin.php");
				$userid = $_GET['id'];
				$admin  = new Admin();
				if ($admin->isadmin()) {
					$admin->removeuser($userid);
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				exit;
			}
			
			if (isset($_GET['setadmin'])) {
				require("Classes/admin.php");
				$userid = $_GET['id'];
				$admin  = new Admin();
				
				if ($admin->isadmin()) {
					$admin->setadmin($userid);
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				exit;
			}
			
			if (isset($_GET['removeadmin'])) {
				require("Classes/admin.php");
				$userid = $_GET['id'];
				$admin  = new Admin();
				
				if ($admin->isadmin()) {
					$admin->removeadmin($userid);
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				
				exit;
			}
			
			if (isset($_GET['adminshowuser'])) {
				require("Classes/admin.php");
				$userid = $_GET['id'];
				$admin  = new Admin();
				if ($admin->isadmin()) {
					$admin->adminshowuser($userid);
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				
				exit;
			}
			
			if (isset($_GET['adminedituser'])) {
				require("Classes/admin.php");
				$name   = $_POST['name'];
				$email  = $_POST['email'];
				$userid = $_POST['id'];
				$admin  = new Admin();
				if ($admin->isadmin()) {
					$admin->adminedituser($userid, $name, $email);
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				exit;
			}
			
			if (isset($_GET['useredituser'])) {
				
				$name   = $_POST['name'];
				$email  = $_POST['email'];
				$userid = $_POST['id'];
				
				$user->useredituser($userid, $name, $email);
				exit;
			}
			
			if (isset($_GET['usereditpw'])) {
				
				$passwort      = $_POST['passwort'];
				$passwortagain = $_POST['passwortagain'];
				$userid        = $_SESSION['bmuser'];
				if ($passwort == $passwortagain) {
					$user->changePassword($passwort);
				} else {
					$alert = new Alert("danger", "Passwort stimmt nicht überein");
				}
				exit;
			}
			
			if (isset($_GET['admineditpw'])) {
				require("Classes/admin.php");
				$admin = new admin();
				if ($admin->isadmin()) {
					$id            = $_POST['id'];
					$passwort      = $_POST['passwort'];
					$passwortagain = $_POST['passwortagain'];
					$userid        = $_POST['id'];
					if ($passwort == $passwortagain) {
						$admin->changeUserPasswort($id, $passwort);
					} else {
						$alert = new Alert("danger", "Passwort stimmt nicht überein");
					}
				} else {
					$alert = new Alert("danger", "Keinen Zugriff auf diese Funktion");
				}
				exit;
			}
			
			if (isset($_GET['createpost'])) {
				Echo "GETH";
				exit;
			}
			
			if (isset($_GET['rec'])) {
				$rec = "?" . $_GET['rec'];
				echo "<a href=\"$rec\">Weiter</a>";
				exit;
			}
		}
		
		public function showHome()
		{
			echo "<h1>Willkommen</h1>";
		}
		
		
	}