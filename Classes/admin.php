<?php
	
	class admin extends User
	{
		
		/**
		 * @param null|int $userId
		 *
		 * @return bool
		 */
		function isadmin($userId = null)
		{
			if (is_null($userId)) {
				$userId = $_SESSION['bmuser'];
			}
			$pdo    = static::getDb();
			$do     = $pdo->prepare("SELECT * FROM users WHERE id = :userid");
			$result = $do->execute(array('userid' => $userId));
			$user   = $do->fetch();
			
			return (bool) $user['isadmin'];
		}
		
		function showAccounts()
		{
			$sql = "SELECT * FROM users";
			
			echo $this->render('seiten/anaccount.phtml', array(
				'object' => $this,
				'users'  => static::getDb()->query($sql),
			));
			
		}
		
		function createUser()
		{
			$alert = null;
			if (isset($_POST['name'])) {
				$pdo           = static::getDb();
				$name          = $_POST['name'];
				$email         = $_POST['email'];
				$passwort      = $_POST['passwort'];
				$passwortagain = $_POST['passwortagain'];
				if ($passwort == $passwortagain) {
					$lenght = strlen($passwort);
					if ($lenght > 11) {
						if (preg_match('/[äüöß\!"§\$\%\#\&\/\(\)\=\?\,\.\-_\:;\]\+\*\~<>\|]/', $passwort)) {
							$sql              = "SELECT MAX(ID) FROM users";
							$count            = $pdo->query($sql)->fetch();
							$maxid            = $count[0];
							$salt             = $maxid + 1;
							$passwortuserhash = hash('sha512', $passwort . $salt);
							$do               = $pdo->prepare("INSERT INTO users (email, name, passwort) VALUES (:email, :name, :passwort)");
							$do->execute(array('email' => $email, 'name' => $name, 'passwort' => $passwortuserhash));
							$alert = new Alert("sucess", "Nutzer erfolgreich erstellt");
						} else {
							$alert = new Alert("warning", "Passwort enthält keien Sonderzeichen, bitte passwortvorgabe beachten");
						}
					} else {
						$alert = new Alert("warning", "Passwort zu kurz, bitte Passwortvorgabe beachten");
					}
				} else {
					$alert = new Alert("danger", "Passwort stimmt nicht überein");
				}
			}
			echo $this->render("seiten/reg.phtml", array(
				'alert' => $alert,
			));
			exit;
		}
		
		function removeuser($id)
		{
			if (isset($_GET['id'])) {
				$pdo    = static::getDb();
				$userid = $_GET['id'];
				$stat   = $pdo->prepare("DELETE FROM users WHERE id = :id");
				$stat->execute(array('id' => "$userid"));
				$alert = new Alert("success", "Nutzer entfernt");
				
			} else {
				$alert = new Alert("danger", "Keine ID mitgebene");
				
			}
			include("seiten/admin.phtml");
		}
		
		function setadmin($id)
		{
			if (isset($_GET['id'])) {
				$pdo    = static::getDb();
				$userid = $_GET['id'];
				$do     = $pdo->prepare("UPDATE users SET isadmin = ? WHERE id = ?");
				$do->execute(array('true', "$userid"));
				$alert = new Alert("success", "Adminrechte gebeben");
				
			} else {
				$alert = new Alert("danger", "Keine ID mitgeben");
			}
			include("seiten/admin.phtml");
		}
		
		function removeadmin($id)
		{
			if (isset($_GET['id'])) {
				$pdo    = static::getDb();
				$userid = $_GET['id'];
				$do     = $pdo->prepare("UPDATE users SET isadmin = ? WHERE id = ?");
				$do->execute(array('false', "$userid"));
				$alert = new Alert("success", "Adminrechte entzogen");
				
				
			} else {
				$alert = new Alert("danger", "Keine ID mitgeben");
			}
			include("seiten/admin.phtml");
		}
		
		function adminedituser($id, $name, $email)
		{
			
			$pdo = static::getDb();
			
			$do = $pdo->prepare("UPDATE users SET name = :name, email = :email  WHERE id = :id");
			$do->execute(array(
				'id'    => $id,
				'email' => $email,
				'name'  => $name,
			));
			
			$alert = new Alert("succes", "Geändert");
			$this->showAccounts();
			
		}
		
		function adminshowuser($id)
		{
			$userid = $id;
			$pdo    = static::getDb();
			$sql    = "SELECT * FROM users WHERE id = $userid";
			foreach ($pdo->query($sql) as $row) {
				
				$name    = $row['name'];
				$id      = $row['id'];
				$email   = $row['email'];
				$size    = "100";
				$isadmin = $row['isadmin'];
				
				$grav_url = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($email))) . "?s=" . $size;
				echo $this->render('seiten/admineditaccount.phtml', array(
					'gravurl' => $grav_url,
					'name'    => $name,
					'id'      => $id,
					'email'   => $email,
				
				));
			}
		}
		
		function changeUserPasswort($id, $password)
		{
			
			$pdo              = static::getDb();
			$salt             = $id;
			$passwortuserhash = hash('sha512', $password . $salt);
			$do               = $pdo->prepare("UPDATE users SET passwort = :passwort  WHERE id = :id");
			$do->execute(array(
				'passwort' => $passwortuserhash,
				'id'       => $id,
			));
			$alert = new Alert("success", "Geändert");
			
			include("seiten/admin.phtml");
		}
		
	}
