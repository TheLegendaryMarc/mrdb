<?php
	
	/**
	 * @author     Marc Retz
	 * @company    Beyond Media GmbH
	 */
	abstract class Base
	{
		/**
		 * @var PDO|null
		 */
		protected static $db;
		
		/**
		 * @param PDO|null $db
		 */
		public function __construct(PDO $db = null)
		{
			if (!is_null($db)) {
				static::setDb($db);
			}
		}
		
		/**
		 * @param PDO $db
		 */
		public static function setDb(PDO $db)
		{
			static::$db = $db;
		}
		
		/**
		 * @return null|PDO
		 * @throws InvalidArgumentException
		 */
		public static function getDb()
		{
			if (is_null(static::$db)) {
				throw new InvalidArgumentException('Property "db" not set!');
			}
			
			return static::$db;
		}
		
		/**
		 * @param string $file
		 * @param array  $params
		 *
		 * @return string
		 */
		public function render($file, array $params = array())
		{
			try {
				extract($params);
				ob_start();
				ob_flush();
				include $file;
				$content = ob_get_clean();
			} catch (Exception $exception) {
				// Logging ..
				$content = 'Nichts ..';
			}
			
			return $content;
		}
	}