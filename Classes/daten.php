<?php
	
	class daten extends User
	{
		function showDaten()
		{
			$pdo = static::getDb();
			$sql = "SELECT * FROM sshaccount";
			foreach ($pdo->query($sql) as $row) {
				
				$name     = $row['name'];
				$id       = $row['id'];
				$ip       = $row['ip'];
				$username = $row['username'];
				$passwort = $row['passwort'];
				echo $this->render('seiten/datensatz.phtml', array(
					'id'       => $id,
					'name'     => $name,
					'ip'       => $ip,
					'username' => $username,
					'passwort' => $passwort,
				));
			}
		}
	}
