<?php
	session_start();
	
	class User extends Base
	{
		private $id;
		private $passwort;
		
		public $name, $email;
		
		public function login()
		{
			if (isset($_POST['email'])) {
				if(isset($_GET['rec'])){
					$rec = "?" . $_GET['rec'];
				}else{
					$rec = "?daten";
				}
				
				$passwortuser     = $_POST['password'];
				$email            = $_POST['email'];
				$pdo              = static::getDb();
				$do               = $pdo->prepare("SELECT id, passwort FROM users WHERE email = :email");
				$result           = $do->execute(array('email' => $email));
				$user             = $do->fetch();
				$salt             = $user['id'];
				$passwortusergen  = $passwortuser . $salt;
				$passwortuserhash = hash('sha512', "$passwortusergen");
				$pwhash           = $passwortuserhash;
				$dbpw             = $user['passwort'];
				if ($dbpw == $pwhash) {
					
					$_SESSION['bmuser'] = $user['id'];
					header("Location: $rec");
				} else {
					echo "<div class=\"alert alert-danger\" role=\"alert\">Email oder Passwort falsch</div>";
					include("seiten/login.phtml");
				}
				exit;
			} else {
				echo $this->render('seiten/login.phtml', array(
					'test' => 'woohooo',
				));
				exit;
			}
		}
		
		function showAccount()
		{
			$user  = $this->getUserData();
			$name  = $user['name'];
			$id    = $user['id'];
			$email = $user['email'];
			$size  = "100";
			$grav_url = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($email))) . "?s=" . $size;
			echo $this->render('seiten/myaccount.phtml', array(
				'gravurl' => $grav_url,
				'name'    => $name,
				'id'      => $id,
				'email'   => $email,
			
			));
		}
		
		function isloggedin()
		{
			if (!isset($_SESSION['bmuser'])) {
				$this->login();
			} else {
				$this->getUserData();
			}
		}
		
		function logout()
		{
			session_destroy();
			
			echo "<div class=\"alert alert-info\" role=\"alert\">Du bist nun ausgeloggt</div>";
			$this->render("seiten/login.phtml");
			echo '<meta http-equiv="refresh" content="2; URL=/index.php">';
			exit;
		}
		
		
		function changePassword($password)
		{
			$id     = $_SESSION['bmuser'];
			$pdo    = static::getDb();
			$salt   = $id;
			$lenght = strlen($password);
			if ($lenght > 11) {
				if (preg_match('/[äüöß\!"§\$\%\#\&\/\(\)\=\?\,\.\-_\:;\]\+\*\~<>\|]/', $password)) {
					$passwortuserhash = hash('sha512', $password . $salt);
					$do               = $pdo->prepare("UPDATE users SET passwort = :passwort  WHERE id = :id");
					$do->execute(array(
						'passwort' => $passwortuserhash,
						'id'       => $id,
					));
					echo "<div class=\"alert alert-success\" role=\"alert\">Geändert</div>";
				} else {
					echo "Passwort enthält kein Sonderzeichen, bitte beachten sie die Passwortvorgaben";
				}
				
			} else {
				echo "Passwort ist zu kurz, bitte Passwortvorgaben beachten";
			}
			
			
			$this->showAccount();
		}
		
		
		public function getUserData()
		{
			$userid = $_SESSION['bmuser'];
			$pdo    = static::getDb();
			$do     = $pdo->prepare("SELECT * FROM users WHERE id = :id");
			$result = $do->execute(array('id' => $userid));
			$user   = $do->fetch();
			
			return $user;
		}
		
		function useredituser($id, $name, $email)
		{
			$id  = $_SESSION['bmuser'];
			$pdo = static::getDb();
			
			$do = $pdo->prepare("UPDATE users SET name = :name, email = :email  WHERE id = :id");
			$do->execute(array(
				'id'    => $id,
				'email' => $email,
				'name'  => $name,
			));
			
			echo "<div class=\"alert alert-success\" role=\"alert\">Geändert</div>";
			$this->showAccount();
			
		}
	}